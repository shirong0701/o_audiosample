//
//  audioAppDelegate.h
//  audiosample
//
//  Created by Gentle Man on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class audioViewController;

@interface audioAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) audioViewController *viewController;

@end
