//
//  audioViewController.h
//  audiosample
//
//  Created by Gentle Man on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
@interface audioViewController : UIViewController{
    IBOutlet UIButton   *recButton;
    
    NSTimer             *scheduleTimer;
    AVAudioPlayer       *player1;
    AVAudioPlayer       *player2;
    AVAudioPlayer       *player3;
    
    AVAudioRecorder     *recorder1;
    AVAudioRecorder     *recorder2;
    AVAudioRecorder     *recorder3;
    
    NSURL               *url1;
    NSURL               *url2;
    NSURL               *url3;
@public
    BOOL                isRecord;
    float               timerValue;
    int                 currentStep;
}
@property (nonatomic, retain) UIButton            *recButton;
@property (nonatomic, retain) NSTimer             *scheduleTimer;
@property (nonatomic, retain) AVAudioPlayer       *player1;
@property (nonatomic, retain) AVAudioPlayer       *player2;
@property (nonatomic, retain) AVAudioPlayer       *player3;
@property (nonatomic, retain) AVAudioRecorder     *recorder1;
@property (nonatomic, retain) AVAudioRecorder     *recorder2;
@property (nonatomic, retain) AVAudioRecorder     *recorder3;

@property (nonatomic, retain) NSURL               *url1;
@property (nonatomic, retain) NSURL               *url2;
@property (nonatomic, retain) NSURL               *url3;

-(IBAction)recOrStop:(id)sender;
@end
