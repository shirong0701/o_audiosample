//
//  audioViewController.m
//  audiosample
//
//  Created by Gentle Man on 5/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "audioViewController.h"

@implementation audioViewController

@synthesize recButton;
@synthesize scheduleTimer;
@synthesize player1, player2, player3;
@synthesize recorder1, recorder2, recorder3;
@synthesize url1, url2, url3;

-(void)updateTimer
{
    timerValue += 0.1f;
    
    if (timerValue >= 3.0f) {
        timerValue = 0.f;
        
        if (currentStep == 1) {
            [recorder1 stop];
            [recorder2 record];
        
            player1 = [[AVAudioPlayer alloc] initWithContentsOfURL:url1 error:nil];
            [player1 prepareToPlay];
            [player1 play];
            
            if (player2) {
                [player2 release];
                player2 = nil;
            }
            currentStep = 2;
        }
        else{
            [recorder2 stop];
            [recorder1 record];
            
            player2 = [[AVAudioPlayer alloc] initWithContentsOfURL:url2 error:nil];
            [player2 prepareToPlay];
            [player2 play];
            
            if (player1) {
                [player1 release];
                player1 = nil;
            }
            currentStep = 1;
        }
    }
}

-(IBAction)recOrStop:(id)sender{
    if (isRecord) {
        isRecord = NO;
        timerValue = 0.f;
        currentStep = 1;
        
        [recButton setTitle:@"REC" forState:UIControlStateNormal];
        [recButton setTitle:@"REC" forState:UIControlStateHighlighted];
        
        [recorder1 stop];
        [recorder2 stop];
        
        if (player1) {
            [player1 stop];
            [player1 release];
            player1 = nil;
        }
        
        if (player2) {
            [player2 stop];
            [player2 release];
            player2 = nil;
        }
        [scheduleTimer invalidate];
        scheduleTimer = nil;
    }
    else{
        isRecord = YES;
        [recButton setTitle:@"STOP" forState:UIControlStateNormal];
        [recButton setTitle:@"STOP" forState:UIControlStateHighlighted];
        
        scheduleTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f
														 target:self
													   selector:@selector(updateTimer)
													   userInfo:nil
														repeats:YES];
        [recorder1 record];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    isRecord = NO;
    timerValue = 0.f;
    currentStep = 1;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        //return YES;
    }
    UInt32 doChangeDefaultRoute = 1;
	AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
    AudioSessionSetActive(true);
    
    [audioSession setActive:YES error:&err];
    if(err){
        NSLog(@"audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        //return YES;
    }

    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    url1 = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:@"recording1.aiff"]];
    url2 = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:@"recording2.aiff"]];
    url3 = [[NSURL alloc] initFileURLWithPath:[documentDirectory stringByAppendingPathComponent:@"recording3.aiff"]];
    
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] init];
    [recordSettings setValue :[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    [recordSettings setValue :[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSettings setValue :[NSNumber numberWithBool:YES] forKey:AVLinearPCMIsFloatKey];
    [recordSettings setValue :[NSNumber numberWithInt:AVAudioQualityMax] forKey:AVEncoderAudioQualityKey];
    [recordSettings setValue :[NSNumber numberWithInt: kAudioFormatAppleIMA4]  forKey:AVFormatIDKey];
    
    ////////////////////////
    NSError *error;
    recorder1 = [[AVAudioRecorder alloc]
                     initWithURL:url1
                     settings:recordSettings
                     error:&error];
    
    
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
        
    } else {
        [recorder1 prepareToRecord];
    }
    ////////////////////////
    recorder2 = [[AVAudioRecorder alloc]
                 initWithURL:url2
                 settings:recordSettings
                 error:&error];
    
    
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
        
    } else {
        [recorder2 prepareToRecord];
    }
    ////////////////////////
    recorder3 = [[AVAudioRecorder alloc]
                 initWithURL:url3
                 settings:recordSettings
                 error:&error];
    
    
    if (error)
    {
        NSLog(@"error: %@", [error localizedDescription]);
        
    } else {
        [recorder3 prepareToRecord];
    }

    
    [recordSettings release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
